package co.edu.eafit.st0257.manejadordeencuentros.impls.crazycontrol;

import co.edu.eafit.st0257.manejadordeencuentros.IRoom;
import co.edu.eafit.st0257.manejadordeencuentros.ProcesoB;
import co.edu.eafit.st0257.manejadordeencuentros.Procesos;

public class ProcesoBImpls extends ProcesoB {

	private Procesos procesoEncuentro;
	private RoomImpls room;
	private boolean listo;

	public ProcesoBImpls(IRoom room, Integer id) {
		super(room, id);
		this.room = (RoomImpls) room;
		this.listo = false;
	}

	@Override
	public void limpiarListaEncuentros() {
		this.procesoEncuentro = null;
	}

	@Override
	public void adicionarEncuentro(Procesos proceso) {
		if (proceso instanceof ProcesoAImpls) {
			this.procesoEncuentro = proceso;
			System.out.println("Yo, el proceso B" + this.obtenerId()
					+ " me voy porque ya me encontre con el proceso A"
					+ proceso.obtenerId());
			this.listo = true;
			this.room.salir(this);
		}
	}

	@Override
	public void run() {
		while (true) {
			this.room.enter(this);
			this.listo=false;
		}
	}

	public boolean estaListo() {
		return listo;
	}
}
