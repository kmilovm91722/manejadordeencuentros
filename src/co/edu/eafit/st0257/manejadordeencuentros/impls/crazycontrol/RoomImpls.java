package co.edu.eafit.st0257.manejadordeencuentros.impls.crazycontrol;

import java.util.ArrayList;

import co.edu.eafit.st0257.manejadordeencuentros.IRoom;
import co.edu.eafit.st0257.manejadordeencuentros.Procesos;

public class RoomImpls implements IRoom {

	private ArrayList<Procesos> procesos;

	public RoomImpls() {
		this.procesos = new ArrayList<Procesos>();
	}

	@Override
	public synchronized void enter(Procesos proceso) {
		procesos.add(proceso);
		imprimirSaludo(proceso);
		notifyAll();
		while (!procesoListo(proceso)) {
			try {
				wait();
				Procesos ultimo = obtenerUltimo();
				proceso.adicionarEncuentro(ultimo);
				ultimo.adicionarEncuentro(proceso);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void salir(Procesos proceso) {
		int id = proceso.obtenerId();
		for (int i = 0; i < procesos.size(); i++) {
			if (procesos.get(i).obtenerId() == id)
				procesos.remove(i);
		}
	}

	// Entrega el ultimo proceso en entrar al cuarto
	public synchronized Procesos obtenerUltimo() {
		return procesos.get(procesos.size() - Integer.valueOf(1));
	}

	private synchronized boolean procesoListo(Procesos proceso) {
		if (proceso instanceof ProcesoAImpls) {
			return ((ProcesoAImpls) proceso).estaListo();
		}
		if (proceso instanceof ProcesoBImpls) {
			return ((ProcesoBImpls) proceso).estaListo();
		}
		return false;
	}

	private void imprimirSaludo(Procesos proceso) {
		char tipo = obtenerTipoProceso(proceso);
		System.out.print("Yo, el proceso " + tipo + proceso.obtenerId()
				+ " acabo de entrar en el cuarto y estoy esperando");
		if (tipo == 'A')
			System.out.println(" dos amigos de tipo B");
		if (tipo == 'B')
			System.out.println(" un amigo de tipo A");
	}

	private char obtenerTipoProceso(Procesos proceso) {
		if (proceso instanceof ProcesoAImpls) {
			return 'A';
		}
		if (proceso instanceof ProcesoBImpls) {
			return 'B';
		}
		return 'n';
	}

}
