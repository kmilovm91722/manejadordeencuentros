package co.edu.eafit.st0257.manejadordeencuentros.impls.crazycontrol;

import co.edu.eafit.st0257.manejadordeencuentros.IRoom;
import co.edu.eafit.st0257.manejadordeencuentros.ProcesoA;
import co.edu.eafit.st0257.manejadordeencuentros.Procesos;

public class ProcesoAImpls extends ProcesoA {

	private static final int CANTIDAD_PROCESOS_A_REUNIR = 2;

	private Procesos[] listaEncuentros;
	private RoomImpls room;
	private boolean listo;

	public ProcesoAImpls(IRoom room, Integer id) {
		super(room, id);
		this.room = (RoomImpls) room;
		this.listaEncuentros = new ProcesoBImpls[CANTIDAD_PROCESOS_A_REUNIR];
		this.listo = false;
	}

	@Override
	public void limpiarListaEncuentros() {
		for (int i = 0; i < listaEncuentros.length; i++) {
			listaEncuentros[i] = null;
		}
	}

	@Override
	public void adicionarEncuentro(Procesos proceso) {
		if (proceso instanceof ProcesoBImpls) {
			if (listaEncuentros[0] != null) {
				listaEncuentros[1] = proceso;
				System.out.println("Yo, el proceso A" + this.obtenerId()
						+ " me voy porque ya me encontre con los procesos B"
						+ listaEncuentros[0].obtenerId() + " y B"
						+ listaEncuentros[1].obtenerId());
				this.listo = true;
				this.room.salir(this);
			} else {
				listaEncuentros[0] = proceso;
				System.out.println("Yo, el proceso A" + this.obtenerId()
						+ " acabo de encontrarme con el proceso B"
						+ proceso.obtenerId() + ", pero me falta uno mas");
			}
		}
	}

	@Override
	public void run() {
		while (true) {
			this.room.enter(this);
			this.listo=false;
		}
	}

	public boolean estaListo() {
		return listo;
	}

}
