package co.edu.eafit.st0257.manejadordeencuentros.impls.crazycontrol;

import co.edu.eafit.st0257.manejadordeencuentros.utils.AbsPrincipal;
import co.edu.eafit.st0257.manejadordeencuentros.utils.InternalException;

public class Principal extends AbsPrincipal {

	protected Principal() throws InternalException {
		super();
	}

	@Override
	protected String obtenerNombrePaquete() {
		return "co.edu.eafit.st0257.manejadordeencuentros.impls.crazycontrol";
	}

	public static void main(String[] args) {
		try {
			AbsPrincipal principal = new Principal();
			principal.ejecutar();
		} catch (InternalException ie) {
			System.err.println(ie);
			System.exit(1);
		}
	}

}
