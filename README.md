Practica de Concurrencia Sistemas Operativos
Universidad Eafit
Un Manejador de Encuentros
===========================

Nombre del equipo: CrazyControl
Integrantes del equipo:
Nombre: Daniel Camilo Vergara Marin        Código: 200820001010
Nombre: Anderson Lopez Monsalve            Código: 200910007010

--------------------------------------
Descripción:
    Dos clases de procesos: A y B entran a un cuarto. Un proceso A no puede dejarlo hasta que se reuna
    con dos procesos tipo B, y un proceso tipo B no puede dejarlo hasta que se reuna con un proceso tipo
    A. Cada clase de proceso deja el cuarto –sin reunirse con ningún otro tipo de proceso– una vez ha
    reunido el número requerido de otros procesos.

--------------------------------------
Como ejecutarlo:

+Situado en la raiz del proyecto, ejecutar:

            $ ant create_run_jar

+El jar generado, queda ubicado por defecto en jar/ManejoDeEncuentrosCrazyControl.jar
+Ejecutar:

            $ java -jar  jar/ManejoDeEncuentrosCrazyControl.jar

Notas:
+Versión de java utilizada: 1.7
+El jar generado trae empaquetado otro jar generado con las clases base creadas por el profesor
Juan Francisco Cardona Mc'Cormick necesarias para el desarrollo de la practica
